// styles
import "./App.css";
// pages
import TitlePage from "./pages/TitlePage/TitlePage";
import ContactPage from "./pages/ContactPage/ContactPage";
import NewsPage from "pages/NewsPage/NewsPage";
import SingleNewsPage from "pages/SingleNewsPage/SingleNewsPage";
// react
import React, { useState, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
// axios
import axios from "axios";
// components
import Layout from "components/Layout/Layout";

const useFetch = () => {
  const [data, updateData] = useState(null);
  const url =
    "https://newsapi.org/v2/top-headlines?" +
    "country=us&" +
    "sortBy=popularity&" +
    "apiKey=92019edca5cd447fa0c087a532dea210";
  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(url);
      updateData(response.data);
    };
    fetchData();
  }, []);
  return data;
};

const App = () => {
  const response = useFetch();
  console.log(response)

  return response?.articles ? (
    <div className="App">
      <Layout>
        <Routes>
          <Route path="/" element={<TitlePage data={response} />} />
          <Route path="/news" element={<NewsPage data={response} />} />
          <Route
            path="/news/:slug"
            element={<SingleNewsPage data={response} />}
          />
          <Route path="/contact" element={<ContactPage />} />
        </Routes>
      </Layout>
    </div>
  ) : (
    ""
  );
};

export default App;