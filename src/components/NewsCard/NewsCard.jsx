// react
import React from "react";
// router
import { NavLink } from "react-router-dom";
// styles
import s from "./NewsCard.module.css";

const getHostname = (link) => {
  const url = new URL(link);
  return url.hostname;
};

const getDate = (fullDate) => {
  const date = new Date(fullDate);
  return date.getDate();
};

const getMonth = (fullDate) => {
  const date = new Date(fullDate);
  return date.getMonth() + 1;
};

const NewsCard = ({ title, href, date }) => {
  return (
    <NavLink
      to={`/news/${title
        .toLowerCase()
        .replace(/[&\\#,+()$~%.'":*?<>{}]/g, "")
        .replace(/ /g, "-")}`}
    >
      <div className={s.cardWrapper}>
        <h2 className={s.cardHeader}>{title}</h2>
        <div className={s.cardInfo}>
          <p>{getHostname(href)}</p>
          <div className={s.cardDate}>
            <span className={s.cardDay}>{getDate(date)}</span>
            <span className={s.cardSplitter}>/</span>
            <span className={s.cardMonth}>{getMonth(date)}</span>
          </div>
        </div>
      </div>
    </NavLink>
  );
};

export default NewsCard;
