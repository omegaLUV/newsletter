// react
import React from "react";
// router
import { NavLink } from "react-router-dom";
// styles
import s from "./Header.module.css";

const Header = () => {
  return (
    <div className={s.headerWrapper}>
      <NavLink to="/" className={s.headerLogo}>
        NewsLetter
      </NavLink>
      <div className={s.headerNav}>
        <NavLink
          exact="true"
          to="/"
          className={({ isActive }) => (isActive ? s.activeLink : null)}
        >
          Title
        </NavLink>
        <NavLink
          exact="true"
          to="/news"
          className={({ isActive }) => (isActive ? s.activeLink : null)}
        >
          News
        </NavLink>
        <NavLink
          exact="true"
          to="/contact"
          className={({ isActive }) => (isActive ? s.activeLink : null)}
        >
          Contact
        </NavLink>
      </div>
    </div>
  );
};

export default Header;