// react
import React from "react";
// router
import { NavLink } from "react-router-dom";
// styles
import s from "./Footer.module.css";

const Footer = () => {
  return (
    <div className={s.footerWrapper}>
      <div className={s.footerLogo}>
        <NavLink to="/">NewsLetter</NavLink>
        <span>Single Page Application</span>
      </div>
      <p className={s.footerText}>Project</p>
      <div className={s.footerBy}>
        <span>Made by</span>
        <p>Matvei Belousov</p>
      </div>
    </div>
  );
};

export default Footer;
