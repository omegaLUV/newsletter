// react
import React from "react";
// styles
import s from "./ContactPage.module.css";

const ContactPage = () => {
  return (
    <div className={s.pageWrapper}>
      <div className={s.pageInfo}>
        <div className={s.infoNumber}>
          <a href="tel:+420773946437">+420 773 946 437</a>
        </div>
        <p className={s.infoName}>Matvei Belousov</p>
        <div className={s.infoMail}>
          <a href="mailto:bel.mot50@gmail.com">bel.mot50@gmail.com</a>
        </div>
        <p className={s.infoProf}>Javascript developer</p>
        <p className={s.infoTech}>
          ES5, ES6, <span>React</span>
        </p>
      </div>
      <div className={s.pagePhoto}>
        <img src="/images/me.jpg" />
      </div>
    </div>
  );
};

export default ContactPage;
