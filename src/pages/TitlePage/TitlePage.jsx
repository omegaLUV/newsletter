// react
import React, { useState } from "react";
// components
import NewsCard from "../../components/NewsCard/NewsCard";
// styles
import s from "./TitlePage.module.css";

const TitlePage = ({ data }) => {
  const newArr = new Array(data.articles);
  const fullArr = new Array(newArr[0]);
  const spliceArr = new Array(newArr[0].slice(0, 6));

  const [arr, setArr] = useState(spliceArr);
  const [isAll, setAll] = useState(false);

  const renderNews = () => {
    setAll(!isAll);
    if (isAll === false) {
      setArr(fullArr);
    } else {
      setArr(spliceArr);
    }
  };

  return (
    <div className={s.pageWrapper}>
      <h1 className={s.pageHeader}>
        Always <br />
        fresh <span>news</span>
      </h1>
      <div className={s.pageNews}>
        {data !== null
          ? arr[0].map((card, index) => {
              return (
                <NewsCard
                  key={index}
                  title={card.title}
                  href={card.url}
                  date={card.publishedAt}
                />
              );
            })
          : null}
      </div>
      <button className={s.pageBtn} onClick={renderNews}>
        {!isAll ? "More news" : "Less news"}
      </button>
    </div>
  );
};

export default TitlePage;
