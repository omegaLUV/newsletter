// react
import React from "react";
// components
import NewsCard from "components/NewsCard/NewsCard";
// styles
import s from "./NewsPage.module.css";

const NewsPage = ({ data }) => {
  return (
    <div className={s.pageWrapper}>
      <h1 className={s.pageHeader}>
        Keep up to date with <span>news</span>
      </h1>
      <div className={s.pageNews}>
        {data !== null
          ? data.articles.map((item, index) => {
              return (
                <NewsCard
                  key={index}
                  title={item.title}
                  href={item.url}
                  date={item.publishedAt}
                />
              );
            })
          : null}
      </div>
    </div>
  );
};

export default NewsPage;
