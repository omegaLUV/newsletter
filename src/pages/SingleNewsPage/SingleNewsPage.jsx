// react
import React from "react";
// router
import { useMatch } from "react-router-dom";
// styles
import s from "./SingleNewsPage.module.css";

const SingleNewsPage = ({ data }) => {
  let news;
  let match = useMatch("/news/:filename");

  data &&
    data.articles.map((item, index) => {
      const itemName = item.title
        .toLowerCase()
        .replace(/[&\\#,+()$~%.'":;*?<>{}—‘’]/g, "")
        .replace(/ /g, "-");
      if (itemName === match.params.filename) {
        news = item;
      }
    });

  const getHostname = (link) => {
    const url = new URL(link);
    return url.hostname;
  };

  const getDate = (fullDate) => {
    const date = new Date(fullDate);
    return date.getDate();
  };

  const getMonth = (fullDate) => {
    const date = new Date(fullDate);
    return date.getMonth() + 1;
  };

  return (
    <div className={s.pageWrapper}>
      <div className={s.pageDetails}>
        <h2 className={s.detailsHeader}>{news.title}</h2>
        <a className={s.detailsLink} href={news.url} target="_blank">
          {getHostname(news.url)}
        </a>
        <div className={s.detailsDate}>
          <span className={s.detailsDay}>{getDate(news.publishedAt)}</span>
          <span className={s.detailsSplitter}>/</span>
          <span className={s.detailsMonth}>{getMonth(news.publishedAt)}</span>
        </div>
      </div>
      <div className={s.pageContent}>
        <figure className={s.contentImg}>
          <img src={news.urlToImage} />
        </figure>
        <p className={s.contentText}>{news.content}</p>
      </div>
    </div>
  );
};

export default SingleNewsPage;
